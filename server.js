const express = require('express');

// server
const app = express();

app.use(express.static('public'));

app.get('/', (request, response) => {
    response.sendFile(__dirname + '/views/index.html');
});

app.post('/git', (request, response) => {
    // console.log(request.headers);
    
    if (request.headers['x-gitlab-event'] === 'Push Hook' && request.headers['x-gitlab-token'] === process.env.GIT) {
        response.sendStatus(200);
        
        const { execSync } = require('child_process');
        const commands = [
            'git fetch origin master',
            'git reset --hard origin/master',
            'git pull origin master --force',
            'npm install',
            'refresh' // refresh glitch UI
        ];
        for (const cmd of commands) {
            console.log(execSync(cmd).toString());
        }
        console.log('Successfully updated!');
        return;
    }
    else {
        console.log('Webhook signature incorrect!');
        return response.sendStatus(403);
    }
});

const listener = app.listen(process.env.PORT, () => {
    console.log(`Your app is listening on port ${listener.address().port}`);
});
