# glitch-cd-template

A template to get Gitlab to Glitch Continuous Deployment in 5 steps.

Based on [Continuous deployment to Glitch via Github Webhooks](https://dev.to/healeycodes/continuous-deployment-to-glitch-via-github-webhooks-3hmh).  
This uses a NodeJS Express server with a `/git` route configured to accept `POST` requests from Gitlab webhooks.

## How-to
1. Remix the [Glitch template project](https://glitch.com/edit/#!/gitlab-cd-template)
2. On your remix, open the `.env` file and set `GIT=<your-(generated)-password>`
3. Fork the [glitch-cd-template](https://gitlab.com/makio135/glitch-cd-template) repo on Gitlab
4. In your fork, go to `Settings -> integrations` and create a new webhook:
    - url: `https://<your-project>.glitch.me/git`
    - secret token: `<your-(generated)-password>` (the same as in the `.env`)
    - keep **push events** as triggers
    - click the `Add webhook` button
5. Go back to your Glitch remix and open the console (`tools` button) and type `git remote set-url origin https://gitlab.com/<username||org>/<your-fork>.git` (on your fork, `clone` button and copy `HTTP` address)

That's it! On each future commit, your Glitch project will automatically pull from your fork.

![enjoy](https://media.giphy.com/media/rjkJD1v80CjYs/giphy-downsized.gif)

## Important note
**Your Gitlab repo has to remain public to keep working.**
